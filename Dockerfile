FROM node:latest as builder
RUN mkdir -p /app
COPY ./ ./app
WORKDIR /app 
RUN npm i
RUN npm run build

FROM nginx:latest
RUN mkdir -p /app
WORKDIR /app 
COPY --from=builder /app/dist ./web
RUN sed -i '/index  index.html index.htm/a try_files $uri $uri/ /index.html;'  /etc/nginx/conf.d/default.conf
RUN sed -i 's#/usr/share/nginx/html#/app/web/#g'  /etc/nginx/conf.d/default.conf
CMD ["nginx", "-g", "daemon off;"]