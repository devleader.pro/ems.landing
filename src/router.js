import Vue from 'vue'
import Router from 'vue-router'
import MainPage from "./components/MainPage"
import ProductsPage from "./components/ProductsPage"
import PartnersPage from "./components/PartnersPage"
import ServicesPage from "./components/ServicesPage"
import AboutPage from "./components/AboutPage"

Vue.use(Router);

export default  new Router({
    mode: 'history',
    routes: [
        {
            path: '',
            component: MainPage
        },
        {
            path: '/home',
            component: MainPage
        },
        {
            path: '/products',
            component:  ProductsPage
        },
        {
            path: '/partners',
            component: PartnersPage
        },
        {
            path: '/services',
            component: ServicesPage
        },
        {
            path: '/about',
            component: AboutPage
        }
    ],
    scrollBehavior() {
        return {x: 0, y: 0}
    }
})
